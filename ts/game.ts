class Game {
	//attr
	private _element: HTMLElement = document.getElementById('container');
	private _car: Car;
	private _car2: Car;
	private _parking_lot: Lot;
	private _parking_lot2: Lot;
	private _scoreboard: Scoreboard;
	private _scoreboard2: Scoreboard;
	private _gameMusic1: HTMLAudioElement = new Audio('assets/music/bg_music.mp3');
	private _gameMusic2: HTMLAudioElement = new Audio('assets/music/bg_music2.wav');
	private _gameMusic3: HTMLAudioElement = new Audio('assets/music/win_music.mp3');

	/**
	 * Create the Game class
	 */
	constructor() {
		//create some gameItems
		this._car = new Car('red_car', 0, 220);
		this._car2 = new Car('white_car', 1, -210);
		this._parking_lot = new Lot('parking_lot', 0, 280);
		this._parking_lot2 = new Lot('parking_lot', 1, -145);
		this._scoreboard = new Scoreboard('scoreboard');
		this._scoreboard2 = new Scoreboard('scoreboard2');

		//add some eventlisteners
		window.addEventListener('keydown', this.keyDownHandler);
		this._gameMusic1.addEventListener('ended', this._gameMusic1.play);
		this._gameMusic2.addEventListener('ended', this._gameMusic2.play);

		//draw the objects
		this.draw();

		//play soundtrack 1 
		this._gameMusic1.play();
		this._gameMusic1.volume = 0.5;
		console.log("Music 1 started");
	}

	/**
	 * Function to detect collision between two objects
	 */
	public collision(): void {
		//use elem.getBoundingClientRect() to get the measurements of the element
		const parking_lotRect = document.getElementById('parking_lot-0').getBoundingClientRect();
		const parking_lot2Rect = document.getElementById('parking_lot-1').getBoundingClientRect();
		const carRect = document.getElementById('red_car').getBoundingClientRect();
		const car2Rect = document.getElementById('white_car').getBoundingClientRect();
		if (parking_lotRect.bottom >= carRect.bottom) {
			//red car wins this round, reset positions
			this.round_reset();
			this._scoreboard.addScore();
		}
		if (parking_lot2Rect.bottom >= car2Rect.bottom) {
			//white car wins this round, reset positions
			this.round_reset();
			this._scoreboard2.addScore();
		}
	}

	/**
	 * Function that resets the cars positions
	 */
	private round_reset(): void {
		this._car2.move(this._car2.yPosition);
		this._car.move(this._car.yPosition);
	}

	/**
	 * Function to draw the initial state of al living objects
	 */
	public draw(): void {
		this._car.draw(this._element);
		this._car2.draw(this._element);
		this._parking_lot.draw(this._element);
		this._parking_lot2.draw(this._element);
		this._scoreboard.draw(this._element);
		this._scoreboard2.draw(this._element);
	}

	/**
	 * Function to update all living objects
	 */
	public update(): void {
		this.collision();
		this._car.update();
		this._car2.update();
		this._parking_lot.update();
		this._parking_lot2.update();
		this._scoreboard.update();
		this._scoreboard2.update();
		this.end();
	}

	/**
	 * Function to handle the keystrokes
	 * @param {KeyboardEvent} - event
	 */
	public keyDownHandler = (e: KeyboardEvent): void => {
		if (e.keyCode === 79) {
			//move the red car 25px up
			this._car.move(25);
			this.update();
		}
		if (e.keyCode === 76) {
			//check that the car doesn't go out of the window
			if (this._car.yPosition > -25) {
				console.log("Did not move! Out of window");
			} else {
				//move the red car 25px down
				this._car.move(-25);
				this.update();
			}
		}
		if (e.keyCode === 87) {
			//move the white car 25px up
			this._car2.move(25);
			this.update();
		}
		if (e.keyCode === 83) {
			//check that the car doesn't go out of the window
			if (this._car2.yPosition > -25) {
				console.log("Did not move! Out of window");
			} else {
				//move the white car 25px down
				this._car2.move(-25);
				this.update();
			}
		}
		if (e.keyCode === 49) {
			//play soundtrack 1
			this._gameMusic2.pause();
			this._gameMusic1.play();
			this._gameMusic1.volume = 0.5;
			console.log("Music 1 started");
		}
		if (e.keyCode === 50) {
			//play soundtrack 2
			this._gameMusic1.pause();
			this._gameMusic2.play();
			this._gameMusic2.volume = 0.5;
			console.log("Music 2 started");
		}
		if (e.keyCode === 51) {
			//pause soundtracks
			this.pause();
		}
	}

	/**
	 * Function that pauses all the music
	 */
	private pause(): void {
		this._gameMusic1.pause();
		this._gameMusic2.pause();
	}

	/**
	 * Function that detects if the game has ended and ends the game if so
	 */
	public end() {
		if (this._scoreboard.getScore >= 3) {
			//red car wins
			console.log("Red Won!");
			window.removeEventListener('keydown', this.keyDownHandler);
			this.pause();
			this._gameMusic3.play();
			document.getElementById("container").innerHTML = `<h1 class="rood">Rood heeft gewonnen</h1>`;
		}
		if (this._scoreboard2.getScore >= 3) {
			//white car wins
			console.log("White Won!");
			window.removeEventListener('keydown', this.keyDownHandler);
			this.pause();
			this._gameMusic3.play();
			document.getElementById("container").innerHTML = `<h1>Wit heeft gewonnen</h1>`;
		}
	}
}