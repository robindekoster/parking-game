/// <reference path="gameItem.ts" />

class Car extends GameItem {
    private _id: number;

    /**
    * Function to create the car
    * @param {string} - name
    * @param {number} - id
    * @param {number} - xPosition
    * @param {number} - yPosition
    */
    constructor(name: string, id: number, xPosition: number = 0, yPosition: number = 0) {
        super(name, xPosition, yPosition);
        this._id = id;
    }

    /**
     * Returns the y-Position of the car
     */
    public get yPosition(): number {
        return this._yPos;
    }

    /**
    * Function to move the car
    * @param {number} - yPosition
    */
    public move(yPosition: number): void {
        this._yPos -= yPosition;
        this._element.classList.add('driving');
        console.log("Car moved to yPosition: " + this.yPosition + ".");
    }
}