class GameItem {
    constructor(name, xPosition = 0, yPosition = 0) {
        this._name = name;
        this._xPos = xPosition;
        this._yPos = yPosition;
    }
    set xPos(xPosition) {
        this._xPos = xPosition;
    }
    set yPos(yPosition) {
        this._yPos = yPosition;
    }
    draw(container) {
        this._element = document.createElement('div');
        this._element.className = this._name;
        this._element.id = this._name;
        this._element.style.transform = `translate(${this._xPos}px, ${this._yPos}px)`;
        const image = document.createElement('img');
        image.src = `./assets/img/${this._name}.png `;
        this._element.appendChild(image);
        container.appendChild(this._element);
    }
    update() {
        this._element.style.transform = `translate(${this._xPos}px, ${this._yPos}px)`;
    }
}
class Car extends GameItem {
    constructor(name, id, xPosition = 0, yPosition = 0) {
        super(name, xPosition, yPosition);
        this._id = id;
    }
    get yPosition() {
        return this._yPos;
    }
    move(yPosition) {
        this._yPos -= yPosition;
        this._element.classList.add('driving');
        console.log("Car moved to yPosition: " + this.yPosition + ".");
    }
}
class Game {
    constructor() {
        this._element = document.getElementById('container');
        this._gameMusic1 = new Audio('assets/music/bg_music.mp3');
        this._gameMusic2 = new Audio('assets/music/bg_music2.wav');
        this._gameMusic3 = new Audio('assets/music/win_music.mp3');
        this.keyDownHandler = (e) => {
            if (e.keyCode === 79) {
                this._car.move(25);
                this.update();
            }
            if (e.keyCode === 76) {
                if (this._car.yPosition > -25) {
                    console.log("Did not move! Out of window");
                }
                else {
                    this._car.move(-25);
                    this.update();
                }
            }
            if (e.keyCode === 87) {
                this._car2.move(25);
                this.update();
            }
            if (e.keyCode === 83) {
                if (this._car2.yPosition > -25) {
                    console.log("Did not move! Out of window");
                }
                else {
                    this._car2.move(-25);
                    this.update();
                }
            }
            if (e.keyCode === 49) {
                this._gameMusic2.pause();
                this._gameMusic1.play();
                this._gameMusic1.volume = 0.5;
                console.log("Music 1 started");
            }
            if (e.keyCode === 50) {
                this._gameMusic1.pause();
                this._gameMusic2.play();
                this._gameMusic2.volume = 0.5;
                console.log("Music 2 started");
            }
            if (e.keyCode === 51) {
                this.pause();
            }
        };
        this._car = new Car('red_car', 0, 220);
        this._car2 = new Car('white_car', 1, -210);
        this._parking_lot = new Lot('parking_lot', 0, 280);
        this._parking_lot2 = new Lot('parking_lot', 1, -145);
        this._scoreboard = new Scoreboard('scoreboard');
        this._scoreboard2 = new Scoreboard('scoreboard2');
        window.addEventListener('keydown', this.keyDownHandler);
        this._gameMusic1.addEventListener('ended', this._gameMusic1.play);
        this._gameMusic2.addEventListener('ended', this._gameMusic2.play);
        this.draw();
        this._gameMusic1.play();
        this._gameMusic1.volume = 0.5;
        console.log("Music 1 started");
    }
    collision() {
        const parking_lotRect = document.getElementById('parking_lot-0').getBoundingClientRect();
        const parking_lot2Rect = document.getElementById('parking_lot-1').getBoundingClientRect();
        const carRect = document.getElementById('red_car').getBoundingClientRect();
        const car2Rect = document.getElementById('white_car').getBoundingClientRect();
        if (parking_lotRect.bottom >= carRect.bottom) {
            this.round_reset();
            this._scoreboard.addScore();
        }
        if (parking_lot2Rect.bottom >= car2Rect.bottom) {
            this.round_reset();
            this._scoreboard2.addScore();
        }
    }
    round_reset() {
        this._car2.move(this._car2.yPosition);
        this._car.move(this._car.yPosition);
    }
    draw() {
        this._car.draw(this._element);
        this._car2.draw(this._element);
        this._parking_lot.draw(this._element);
        this._parking_lot2.draw(this._element);
        this._scoreboard.draw(this._element);
        this._scoreboard2.draw(this._element);
    }
    update() {
        this.collision();
        this._car.update();
        this._car2.update();
        this._parking_lot.update();
        this._parking_lot2.update();
        this._scoreboard.update();
        this._scoreboard2.update();
        this.end();
    }
    pause() {
        this._gameMusic1.pause();
        this._gameMusic2.pause();
    }
    end() {
        if (this._scoreboard.getScore >= 3) {
            console.log("Red Won!");
            window.removeEventListener('keydown', this.keyDownHandler);
            this.pause();
            this._gameMusic3.play();
            document.getElementById("container").innerHTML = `<h1 class="rood">Rood heeft gewonnen</h1>`;
        }
        if (this._scoreboard2.getScore >= 3) {
            console.log("White Won!");
            window.removeEventListener('keydown', this.keyDownHandler);
            this.pause();
            this._gameMusic3.play();
            document.getElementById("container").innerHTML = `<h1>Wit heeft gewonnen</h1>`;
        }
    }
}
let app;
(function () {
    let init = function () {
        app = new Game();
    };
    window.addEventListener('load', init);
})();
class Lot extends GameItem {
    constructor(name, id, xPosition = 0, yPosition = 0) {
        super(name, xPosition, yPosition);
        this._id = id;
    }
    draw(container) {
        this._element = document.createElement('div');
        this._element.className = this._name;
        this._element.id = `${this._name}-${this._id}`;
        this._element.style.transform = `translate(${this._xPos}px, ${this._yPos}px)`;
        const image = document.createElement('img');
        image.src = `./assets/img/${this._name}.png `;
        this._element.appendChild(image);
        container.appendChild(this._element);
    }
    remove(container) {
        const elem = document.getElementById(`${this._name}-${this._id}`);
        container.removeChild(elem);
    }
}
class Scoreboard extends GameItem {
    constructor(name) {
        super(name);
        this._score = 0;
    }
    get score() {
        return this._score;
    }
    draw(container) {
        this._element = document.createElement('div');
        this._element.className = this._name;
        this._element.id = this._name;
        const p = document.createElement('p');
        p.innerHTML = 'Score: ';
        const span = document.createElement('span');
        span.innerHTML = this._score.toString();
        p.appendChild(span);
        this._element.appendChild(p);
        container.appendChild(this._element);
    }
    update() {
        const scoreSpan = this._element.childNodes[0].childNodes[1];
        scoreSpan.innerHTML = this._score.toString();
    }
    addScore() {
        this._score += 1;
    }
    get getScore() {
        return this._score;
    }
}
//# sourceMappingURL=main.js.map